from .event import Event1
import datetime

class LookMontreEvent(Event1):
    NAME = "look_montre"

    def perform(self):
        if self.actor.container().has_prop("passe"):
            self.inform("look_montre.passe")
        elif self.actor.container().has_prop("present"):
            self.inform("look_montre.present")
        elif self.actor.container().has_prop("futur"):
            self.inform("look_montre.futur")

    def look_montre_failed(self):
        self.inform("look_montre.failed")