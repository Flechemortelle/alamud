from .event import Event2

class DrinkEvent(Event2):
    NAME = "drink"

    def perform(self):
        self.inform("drink")

    def drink_failed(self):
        self.fail()
        self.inform("drink.failed")