from .event import Event2

class EatEvent(Event2):
    NAME = "eat"

    def perform(self):
        self.inform("eat")

    def eat_failed(self):
        self.fail()
        self.inform("eat.failed")