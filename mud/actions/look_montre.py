from .action import Action1
from mud.events import LookMontreEvent

class LookMontreAction(Action1):
    EVENT = LookMontreEvent
    ACTION = "look_montre"